﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkFIlenameEdit.Core.Filter
{
    [DisplayName("Split")]
    [Description("Split the string and take one substring from the result.")]
    class SplitFilter : Filter
    {
        public override string Info
        {
            get
            {
                return $"Split string : \"{SplitCharacter}\", Element at : {ListIndex}.";
            }
        }

        public override string Execute(string input)
        {
            var split = input.Split(SplitCharacter.ToCharArray());
            var index = ListIndex;
            var isForward = true;
            var output = "";
            if (index < 0)
            {
                index = split.Length + index;
                isForward = false;
            }

            if (index < 0 || index >= split.Length)
            {
                if (!isOverflowLock)
                {
                    return base.Execute(output);
                }
                index = index < 0 ? 0 : index;
                index = split.Length <= index ? split.Length-1 : index;
            }

            if (isTakeAll)
            {
                if (isForward)
                {
                    output = string.Join(SplitCharacter, split.Skip(index));
                }
                else
                {
                    output = string.Join(SplitCharacter, split.Take(index + 1));
                }
            }
            else
            {
                output = split[index];
            }

            return base.Execute(output);
        }

        public override bool Validate(ref string ErrorMsg)
        {
            ErrorMsg = "";

            if (string.IsNullOrEmpty(SplitCharacter))
            {
                ErrorMsg = "Split filter required character to works.";
            }
            else
            {
                return base.Validate(ref ErrorMsg);
            }

            return false;
        }

        [DisplayName("Split Character")]
        [Description("List character used to split the string without any space.")]
        public string SplitCharacter { get; set; }

        [DisplayName("List Index")]
        [Description("Index of the string selected from the splitted result, negative value to start from behind.")]
        public int ListIndex { get; set; }

        [DisplayName("Overflow Lock")]
        [Description("Take the last string if index provided out of range, else ignore and return empty string.")]
        public bool isOverflowLock { get; set; }

        [DisplayName("Take All From Index")]
        [Description("Enable to take sub string starting from the starting index.")]
        public bool isTakeAll { get; set; }
    }
}
